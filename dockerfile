FROM python:3.7
ADD ./connect_influx.py /
ADD ./requirements.txt /
ADD ./yandex-moscow-exchange.xlsx /
RUN pip install -r requirements.txt
CMD python connect_influx.py
