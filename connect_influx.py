import pandas as pd
from influxdb import InfluxDBClient
 
tab = pd.read_excel('yandex-moscow-exchange.xlsx')
graph = []
for index, row in tab.iterrows():
    time = row['Дата'].split('.')[::-1]
    time = '-'.join(time) + "T12:00:00Z"
    graph.append({'time': time,
        'measurement': 'price',
        'fields': {
           'MIN': row['Цена min'],
           'AVG': row['Цена avg'],
           'MAX': row['Цена max']
        }})
 
client = InfluxDBClient(host='influxdb', port=8086,
                       username='yana', password='yana', database='stocks')
client.create_database('stocks')
client.write_points(graph)

