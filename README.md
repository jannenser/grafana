## Выгрузка данных о курсах акций компании Yandex ##
Excel файл с данными: yandex-moscow-exchange
Берем оттуда по дате среднюю, минимальную и максимальную стоимости (за календарный день) акций Яндекса.

0. `docker network create demo`
1. Запускаем influxdb:
    `docker run --rm -it --net=demo --name influxdb influxdb`
2. Запускаем grafana:
    `docker run --rm -it --net=demo -p 127.0.0.1:3000:3000 grafana/grafana`
3. Создаем контейнер и запускаем:
    `docker build -t foo ./имя-папки`
    `docker run --net=demo  --rm -it foo`
4. Дальше открываем в браузере ссылку `http://localhost:3000` и рисуем графики
(Например, среднюю стоимость за весь период скачанных данных,
отдельно - максимальные и минимальные стоимости в день за весь период,
либо - максимальный скачок цены за день (max - min))